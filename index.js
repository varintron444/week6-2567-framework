const http = require('http');
const app = http.createServer((req, res) => {
    if (req.url === '/') {
        res.writeHead(200, { "Content-Type": "text/html" });
        res.write("<h3>Home page</h3>");
        res.end();
    } else if (req.url === "/about") {
         res.writeHead(200, { "Content-Type": "text/plain" });
        res.write("About");
        res.end();
    }
     else if (req.url === "/Admin") {
         res.writeHead(200, { "Content-Type": "text/html" });
        res.write("<h3>Admin</h3>");
        res.end();
    }
    else {
         res.writeHead(200, { "Content-Type": "text/html" });
        res.write("<h style='color=red'><center>404</center></h>");
        res.end();
    }
});
const PORT = 3000;
app.listen(PORT, () => {
    console.log(`Server running at http://localhost:${PORT}`);
});
